import React from 'react';

import './App.scss'

import { ImageProvider } from './Contexts/ImageContext';
import { MainApp } from './Components/MainApp';

function App() {
	return (
		<ImageProvider>
			<MainApp />
		</ImageProvider>
	);
}

export default App;
