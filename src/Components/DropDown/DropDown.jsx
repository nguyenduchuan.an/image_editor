import React, { useState } from 'react'

import './DropDown.scss'
import { useContext } from 'react';
import { TextContext } from '../../Contexts/TextContext';

const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
    zIndex: '100'
}

export const DropDown = (props) => {
    const [drop, setDrop] = useState(false);
    const [val, setVal] = useState(props.title)
    const content = props.content;
    const textContext = useContext(TextContext);
    const handleClick = (e) => {
        setVal(e.target.dataset.content);
        if(props.getVal){
            props.getVal(props.typeDrop, e.target.dataset.content)
        }
        switch (props.typeDrop) {
            case '1':
                textContext.setFontSize(e.target.dataset.content)
                console.log(e.target.dataset.content)
                break;
        
            default:
                break;
        }
        setDrop(false);
    }
    const handleClose = () => {
        setDrop(false);
    };
    return (
        <div className="picker-dropdown">
            <button onClick={() => setDrop(!drop)} type="button" name="" id="" className={ drop ? "btn btn-toogle active" : "btn btn-toogle"}>
                <span>{val}</span>
                <svg xmlns="http://www.w3.org/2000/svg" width={10} height={6} viewBox="0 0 10 6" fill="none">
                    <path d="M1 1L5 5L9 1" stroke="#666666" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
            </button>
            {
                drop ? <div className="dropdown-content"> { content.map((item, key) => <div key={key} className="dropdown-item"><a data-content={item} onClick={handleClick} href="#"> {item} </a></div>) }  </div> : ""
            }
            {
                drop ? <div style={ cover } onClick={ handleClose }/> : ""
            }
        </div>
    )
}
