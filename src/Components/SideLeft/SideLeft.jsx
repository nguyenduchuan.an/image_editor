import React, { useState } from 'react'

import './SideLeft.scss'

import { EditText } from './EditText/EditText'
import { EditIcon } from './EditIcon/EditIcon'
import { EditBg } from './EditBg/EditBg'

const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
    zIndex: '50'
}


export const SideLeft = () => {
    const [editText, setEditText] = useState(false);
    const [editIcon, setEditIcon] = useState(false);
    const [editBg, setEditBg] = useState(false);
    
    const handleClose = () => {
        setEditText(false);
        setEditIcon(false);
        setEditBg(false);
    };
    return (
        <div className="side-left">
            <ul>
                <li>
                    <a className={ editText ? 'editor-option active' : "editor-option" } onClick={() => setEditText(!editText)}>
                        <svg className="svg-fill" xmlns="http://www.w3.org/2000/svg" width={20} height={20} viewBox="0 0 20 20" fill="none">
                        <path d="M13.4356 7.29633H10.532V15.1935H8.57888V7.29633H5.71429V5.71429H13.4356V7.29633Z" fill="#999999" />
                        <rect x="1.45239" y="1.45239" width="17.0952" height="17.0952" stroke="#999999" />
                        <rect width="2.85714" height="2.85714" fill="#999999" />
                        <rect x="17.1429" width="2.85714" height="2.85714" fill="#999999" />
                        <rect x="17.1429" y="17.1429" width="2.85714" height="2.85714" fill="#999999" />
                        <rect y="17.1429" width="2.85714" height="2.85714" fill="#999999" />
                        </svg>
                        <span>Văn bản</span>
                    </a>
                    { editText ? <EditText /> : "" }
                </li>
                <li>
                    <a className={ editIcon ? 'editor-option active' : "editor-option" } onClick={() => setEditIcon(!editIcon)}>
                        <svg className="svg-stroke" xmlns="http://www.w3.org/2000/svg" width={22} height={21} viewBox="0 0 22 21" fill="none">
                            <path d="M11.4755 0.845491C11.4086 0.63948 11.2166 0.5 11 0.5C10.7834 0.5 10.5914 0.63948 10.5245 0.845491L8.39159 7.40983H1.48944C1.27282 7.40983 1.08084 7.54931 1.01391 7.75532C0.94697 7.96133 1.0203 8.18702 1.19554 8.31434L6.7795 12.3713L4.64662 18.9357C4.57968 19.1417 4.65301 19.3674 4.82825 19.4947C5.0035 19.622 5.2408 19.622 5.41604 19.4947L11 15.4377L16.584 19.4947C16.7592 19.622 16.9965 19.622 17.1717 19.4947C17.347 19.3674 17.4203 19.1417 17.3534 18.9357L15.2205 12.3713L20.8045 8.31434C20.9797 8.18702 21.053 7.96133 20.9861 7.75532C20.9192 7.54931 20.7272 7.40983 20.5106 7.40983H13.6084L11.4755 0.845491Z" stroke="#999999" strokeLinejoin="round" />
                        </svg>
                        <span>Biểu tượng</span>
                    </a>
                    { editIcon ? <EditIcon /> : "" }
                </li>
                <li>
                    <a className={ editBg ? 'editor-option active' : "editor-option" } onClick={() => setEditBg(!editBg)}>
                        <svg className="svg-stroke" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                            <rect x="0.5" y="0.5" width="19" height="19" rx="3.5" stroke="#999999"/>
                            <path d="M1.5 18.5L18 1" stroke="#999999"/>
                            <path d="M0.5 14.5L14 0.5" stroke="#999999"/>
                            <path d="M10 19.5L19.5 9.5" stroke="#999999"/>
                            <path d="M5 19.5L19.5 4.5" stroke="#999999"/>
                            <path d="M0.5 10L9.5 0.5" stroke="#999999"/>
                            <path d="M0.5 6L5.5 0.5" stroke="#999999"/>
                            <path d="M14 19.5L19.5 13.5" stroke="#999999"/>
                        </svg>
                        <span>Hình nền</span>
                    </a>
                    { editBg ? <EditBg /> : "" }
                </li>
            </ul>
            {
                editText || editIcon || editBg ? <div style={ cover } onClick={ handleClose }/> : ""
            }
        </div>
    )
}
