import React, {useContext} from 'react'

import imgSample1 from './../../../assets/images/text-style/text-style-1.png'
import imgSample2 from './../../../assets/images/text-style/text-style-2.png'
import imgSample3 from './../../../assets/images/text-style/text-style-3.png'
import imgSample4 from './../../../assets/images/text-style/text-style-4.png'
import imgSample5 from './../../../assets/images/text-style/text-style-5.png'
import imgSample6 from './../../../assets/images/text-style/text-style-6.png'
import imgSample7 from './../../../assets/images/text-style/text-style-7.png'
import imgSample8 from './../../../assets/images/text-style/text-style-8.png'
import imgSample9 from './../../../assets/images/text-style/text-style-9.png'
import imgSample10 from './../../../assets/images/text-style/text-style-10.png'
import imgSample11 from './../../../assets/images/text-style/text-style-11.png'
import imgSample12 from './../../../assets/images/text-style/text-style-12.png'
import imgSample13 from './../../../assets/images/text-style/text-style-13.png'
import imgSample14 from './../../../assets/images/text-style/text-style-14.png'
import { ImageContext } from '../../../Contexts/ImageContext'

export const EditText = () => {
    const imageContext = useContext(ImageContext);
    return (
        <div className="box-dropdown text-editor fade-in">          
            <div className="widg-box">
                <button type="button" name="" id="" className="btn btn-add-text" onClick={() => imageContext.addElmText('vcfont-1')}>
                    <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} viewBox="0 0 16 16" fill="none">
                        <path d="M13.6569 13.6569C12.1459 15.1678 10.1368 16 8 16C5.86316 16 3.85413 15.1678 2.34314 13.6569C0.832153 12.1459 0 10.1368 0 8C0 5.86316 0.832153 3.85413 2.34314 2.34314C3.85413 0.832153 5.86316 0 8 0C10.1368 0 12.1459 0.832153 13.6569 2.34314C15.1678 3.85413 16 5.86316 16 8C16 10.1368 15.1678 12.1459 13.6569 13.6569ZM8 1.25C4.27808 1.25 1.25 4.27808 1.25 8C1.25 11.7219 4.27808 14.75 8 14.75C11.7219 14.75 14.75 11.7219 14.75 8C14.75 4.27808 11.7219 1.25 8 1.25ZM8.625 8.62561H11.4375V7.37561H8.625V4.56311H7.375V7.37561H4.5625V8.62561H7.375V11.4381H8.625V8.62561Z" fill="#999999" />
                    </svg>
                    <span>Thêm văn bản</span>
                </button>
            </div>
            <div className="widg-box">
                <div className="text-concept">
                    <h6 className="title">Mẫu chữ</h6>
                    <div className="list-item">
                        <div className="item">
                            <img src={imgSample1} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample2} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample3} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample4} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample5} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample6} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample7} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample8} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample9} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample10} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample11} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample12} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample13} alt=""/>
                        </div>
                        <div className="item">
                            <img src={imgSample14} alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
