import React, { useContext } from 'react'

import bg1 from './../../../assets/images/background/bg1.png'
import bg2 from './../../../assets/images/background/bg2.png'
import bg3 from './../../../assets/images/background/bg3.png'
import bg4 from './../../../assets/images/background/bg4.png'
import bg5 from './../../../assets/images/background/bg5.png'
import bg6 from './../../../assets/images/background/bg6.png'
import bg7 from './../../../assets/images/background/bg7.png'
import bg8 from './../../../assets/images/background/bg8.png'
import bg9 from './../../../assets/images/background/bg9.png'
import bg10 from './../../../assets/images/background/bg10.png'
import bg11 from './../../../assets/images/background/bg11.png'
import bg12 from './../../../assets/images/background/bg12.png'
import bg13 from './../../../assets/images/background/bg13.png'
import bg14 from './../../../assets/images/background/bg14.png'
import bg15 from './../../../assets/images/background/bg15.png'
import bg16 from './../../../assets/images/background/bg16.png'
import bg17 from './../../../assets/images/background/bg17.png'
import bg18 from './../../../assets/images/background/bg18.png'
import bg19 from './../../../assets/images/background/bg19.png'
import bg20 from './../../../assets/images/background/bg20.png'
import {ChangeColor} from '../../ChangeColor/ChangeColor'
import { ChangeGradient } from '../../ChangeColor/ChangeGradient'
import { ImageContext } from '../../../Contexts/ImageContext'
export const EditBg = () => {
    const imageContext = useContext(ImageContext);
    return (
        <div className="box-dropdown bg-editor fade-in">        
            <div className="widg-box">
                <div className="bg-concept">
                    <h5 className="title">Màu nền</h5>
                    <div className="list-item">
                        <div className="color-item" data-content="#EB5757" style={{background: '#EB5757'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#F2994A" style={{background: '#F2994A'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#F2C94C" style={{background: '#F2C94C'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#219653" style={{background: '#219653'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#2F80ED" style={{background: '#2F80ED'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#56CCF2" style={{background: '#56CCF2'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#9B51E0" style={{background: '#9B51E0'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#D96BA0" style={{background: '#D96BA0'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#000000" style={{background: '#000000'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#7D7D7D" style={{background: '#7D7D7D'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="#FFFFFF" style={{background: '#FFFFFF'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <ChangeColor type="colorMain" />
                    </div>
                    
                </div>
            </div>
            
            <div className="widg-box">
                <div className="bg-concept">
                    <h5 className="title">Màu gradient</h5>
                    <div className="list-item">
                        <div className="color-item" data-content="linear-gradient(180deg, #52ACFF 0%, #FFE32C 100%)" style={{background: 'linear-gradient(180deg, #52ACFF 0%, #FFE32C 100%)'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="linear-gradient(180deg, #FFE53B 0%, #FF2525 100%)" style={{background: 'linear-gradient(180deg, #FFE53B 0%, #FF2525 100%)'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="linear-gradient(180deg, #FAACA8 0%, #DDD6F3 100%)" style={{background: 'linear-gradient(180deg, #FAACA8 0%, #DDD6F3 100%)'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="linear-gradient(180deg, #21D4FD 0%, #B721FF 100%)" style={{background: 'linear-gradient(180deg, #21D4FD 0%, #B721FF 100%)'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="linear-gradient(180deg, #FEE140 0%, #FA709A 100%)" style={{background: 'linear-gradient(180deg, #FEE140 0%, #FA709A 100%)'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="linear-gradient(180deg, #08AEEA 0%, #2AF598 100%)" style={{background: 'linear-gradient(180deg, #08AEEA 0%, #2AF598 100%)'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <div className="color-item" data-content="linear-gradient(199.75deg, #FFFFFF 11.68%, #6284FF 49.85%, #FF3636 88.81%)" style={{background: 'linear-gradient(199.75deg, #FFFFFF 11.68%, #6284FF 49.85%, #FF3636 88.81%)'}} onClick={e => imageContext.addBackGround(e.target.dataset.content)}></div>
                        <ChangeGradient />
                    </div>
                </div>
            </div>

            <div className="widg-box">
                <div className="bg-concept">
                    <h5 className="title">Hình nền</h5>
                    <button type="button" name="" id="" className="btn btn-upload">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                            <path d="M11.386 3.90952C11.2363 3.01995 10.7958 2.2081 10.1221 1.59202C9.37364 0.906848 8.40058 0.529715 7.38721 0.529715C6.60415 0.529715 5.84125 0.754267 5.18774 1.17746C4.64364 1.52869 4.19165 2.0037 3.8721 2.5622C3.73391 2.53629 3.58997 2.5219 3.44602 2.5219C2.2225 2.5219 1.2264 3.51799 1.2264 4.74152C1.2264 4.89986 1.24368 5.05244 1.27247 5.20214C0.480773 5.77792 0 6.70492 0 7.69237C0 8.48982 0.296525 9.26424 0.837754 9.87744C1.39338 10.505 2.12749 10.8764 2.91055 10.9196C2.91919 10.9196 2.92494 10.9196 2.93358 10.9196H5.40942C5.62533 10.9196 5.79807 10.7469 5.79807 10.5309C5.79807 10.315 5.62533 10.1423 5.40942 10.1423H2.9451C1.76763 10.0703 0.777298 8.95044 0.777298 7.68949C0.777298 6.87477 1.21489 6.11475 1.92021 5.70306C2.08431 5.60806 2.1534 5.40942 2.09007 5.23093C2.03249 5.07547 2.0037 4.91137 2.0037 4.73576C2.0037 3.94119 2.65145 3.29344 3.44602 3.29344C3.61588 3.29344 3.78285 3.32223 3.93831 3.37981C4.12832 3.4489 4.33847 3.36253 4.42484 3.18116C4.96319 2.03825 6.12626 1.30125 7.39009 1.30125C9.08863 1.30125 10.4906 2.57372 10.6519 4.26075C10.6691 4.43636 10.8016 4.57742 10.9743 4.60621C12.2554 4.82501 13.2227 6.00823 13.2227 7.35842C13.2227 8.78923 12.0971 10.0329 10.7094 10.1394H8.5877C8.37179 10.1394 8.19905 10.3122 8.19905 10.5281C8.19905 10.744 8.37179 10.9167 8.5877 10.9167H10.7238C10.7325 10.9167 10.7411 10.9167 10.7526 10.9167C11.6307 10.8534 12.4512 10.4503 13.0615 9.77668C13.6689 9.10878 14 8.25087 14 7.35842C13.9971 5.74337 12.8945 4.30681 11.386 3.90952Z" fill="#999999"/>
                            <path d="M9.3334 8.06087C9.48598 7.90829 9.48598 7.66358 9.3334 7.511L7.275 5.4526C7.20303 5.38063 7.10227 5.33745 7.00151 5.33745C6.90074 5.33745 6.79998 5.37775 6.72801 5.4526L4.66961 7.511C4.51703 7.66358 4.51703 7.90829 4.66961 8.06087C4.74446 8.13572 4.84522 8.17602 4.94311 8.17602C5.04099 8.17602 5.14175 8.1386 5.2166 8.06087L6.61286 6.66461V13.0816C6.61286 13.2976 6.78559 13.4703 7.00151 13.4703C7.21742 13.4703 7.39016 13.2976 7.39016 13.0816V6.66461L8.78641 8.06087C8.93612 8.21345 9.18082 8.21345 9.3334 8.06087Z" fill="#999999"/>
                        </svg>
                        <span>Tải lên</span>
                    </button>
                    <div className="list-bgi">
                        <div className="item">
                            <a data-content={bg1} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg1} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg2} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg2} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg3} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg3} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg4} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg4} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg5} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg5} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg6} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg6} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg7} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg7} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg8} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg8} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg9} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg9} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg10} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg10} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg11} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg11} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg12} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg12} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg13} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg13} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg14} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg14} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg15} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg15} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg16} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg16} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg17} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg17} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg18} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg18} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg19} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg19} alt=""/></a>
                        </div>
                        <div className="item">
                            <a data-content={bg20} onClick={e => imageContext.addBackGround(`${"url(" + e.target.src + ")"}`)} href="#"><img src={bg20} alt=""/></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}
