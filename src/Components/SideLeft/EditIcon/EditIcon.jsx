import React from 'react'

import iconSearch from './../../../assets/icon/search.png'
import iconFish from './../../../assets/images/fish.svg'
import {DropDown} from './../../DropDown/DropDown'
import { useContext } from 'react'
import { ImageContext } from '../../../Contexts/ImageContext'

const animals = [
    'Fish',
    'Fish',
    'Fish',
    'Fish',
]

export const EditIcon = () => {
    const imageContext = useContext(ImageContext)
    return (
        <div className="box-dropdown icon-editor fade-in">
            <div className="widg-box">
                <div className="form-group">
                    <input type="text" className="form-control" placeholder="Tìm kiếm" aria-describedby="helpId" />
                    <img className="icon" src={iconSearch} alt=""/>
                </div>
            </div>
            <div className="widg-box">
                <div className="widg-accodition">
                    {/* <button type="button" name="" id="" className="btn btn-toogle">
                        <span>Animals</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width={10} height={6} viewBox="0 0 10 6" fill="none">
                            <path d="M1 1L5 5L9 1" stroke="#666666" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </button> */}
                    <DropDown title="Animals" content={animals} />
                    <div className="acc-content">
                        <div className="list-icon">
                            <div className="icon" data-content={iconFish} onClick={(e) => imageContext.addElmIcon(iconFish)}>
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                        </div>
                        <div className="view-more">
                            <a href="#">Show more</a>
                        </div>
                    </div>
                </div>
                <div className="widg-accodition">
                    {/* <button type="button" name="" id="" className="btn btn-toogle">
                        <span>Business</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width={10} height={6} viewBox="0 0 10 6" fill="none">
                            <path d="M1 1L5 5L9 1" stroke="#666666" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </button> */}
                     <DropDown title="Business" content={animals} />
                    <div className="acc-content">
                        <div className="list-icon">
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                            <div className="icon">
                                <img src={iconFish} alt=""/>
                            </div>
                        </div>
                        <div className="view-more">
                            <a href="#">Show more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
