import React from 'react'

import './HeaderMain.scss'

export const HeaderMain = () => {
    return (
        <div className="header-main">
            <div className="logo">
                <h1>Image Editor</h1>
            </div>
            <div className="header-control">
                <button type="button" name="" id="" className="btn btn-view">
                    <svg xmlns="http://www.w3.org/2000/svg" width={14} height={10} viewBox="0 0 14 10" fill="none">
                        <path d="M7 0.827362C9.67485 0.827362 12.1005 2.2908 13.8905 4.6678C14.0365 4.86255 14.0365 5.13461 13.8905 5.32936C12.1005 7.70923 9.67485 9.17266 7 9.17266C4.32515 9.17266 1.89946 7.70923 0.109543 5.33222C-0.0365143 5.13748 -0.0365143 4.86541 0.109543 4.67067C1.89946 2.2908 4.32515 0.827362 7 0.827362ZM6.80812 7.93834C8.58372 8.05003 10.05 6.58659 9.93832 4.80813C9.84668 3.34184 8.65818 2.15333 7.19188 2.06169C5.41628 1.95 3.94998 3.41343 4.06168 5.19189C4.15618 6.65533 5.34469 7.84383 6.80812 7.93834ZM6.8969 6.58087C7.85343 6.64101 8.64386 5.85344 8.58085 4.89691C8.53217 4.10649 7.89066 3.46785 7.10023 3.4163C6.1437 3.35615 5.35328 4.14372 5.41628 5.10025C5.46783 5.89354 6.10934 6.53218 6.8969 6.58087Z" fill="#666666" />
                    </svg>
                    <span>Xem trước</span>
                </button>
                <button type="button" name="" id="" className="btn btn-save">
                    <svg xmlns="http://www.w3.org/2000/svg" width={14} height={12} viewBox="0 0 14 12" fill="none">
                        <path d="M4.45455 9.10226L1.11364 5.76135L0 6.87499L4.45455 11.3295L14 1.78408L12.8864 0.670441L4.45455 9.10226Z" fill="#666666" />
                    </svg>
                    <span>Lưu ảnh</span>
                </button>
            </div>
            <div className="header-close">
                <button type="button" name="" id="" className="btn btn-close">
                    <svg xmlns="http://www.w3.org/2000/svg" width={22} height={22} viewBox="0 0 22 22" fill="none">
                        <path d="M2 2L20.7383 20.7383" stroke="#666666" strokeWidth="1.5" strokeLinecap="round" />
                        <path d="M20.7383 2L2.00001 20.7383" stroke="#666666" strokeWidth="1.5" strokeLinecap="round" />
                    </svg>
                </button>
            </div>
        </div>
    )
}
