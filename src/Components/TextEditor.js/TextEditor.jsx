import React, {useState, useContext} from 'react'
import InputRange from 'react-input-range'
import './TextEdit.scss'
import 'react-input-range/lib/css/index.css'

import { DropDown } from '../DropDown/DropDown'
import { TextContext } from '../../Contexts/TextContext'
import { ChangeColorEx } from '../ChangeColor/ChangeColorEx'
import { ImageContext } from '../../Contexts/ImageContext'

const font = [
    'Arial',
    'Time New Roman',
    'Roboto'
]
const fontSize = [
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    40,
    50,
    60
]

export const TextEditor = (props) => {
    const [shadow, setShadow] = useState(1);
    const [hoz, setHoz] = useState(1);
    const [vel, setVel] = useState(1);
    const [ShowColorText, setShowColorText] = useState(false)
    const [ShowColorRimText, setShowColorRimText] = useState(false)
    const [ShowColorBorder, setShowColorBorder] = useState(false)
    const [ShowColorRimBorder, setShowColorRimBorder] = useState(false)
    const textContext = useContext(TextContext);
    const imageContext = useContext(ImageContext)

    const getVal = (type, val) => {
        switch (type) {
            case 'Font':
                textContext.setFont(val)
                break;
            case 'FontSize':
                textContext.setFontSize(val)
                break;
            case 'LineHeight':
                textContext.setLineHeight(val)
                break;
            case 'LetterSpacing':
                textContext.setLetterSpacing(val)
                break;
            case 'BorderSize':
                textContext.setBorderSize(val)
                break;
            case 'BTS':
                textContext.setBTS(val)
                break;
            default:
                break;
        }
    }

    return (
        <div className="text-picker fade-in" style={{
            top: `${props.top}px`,
            left: `${props.left}px`,
            visibility: `${props.show ? 'visible' : 'hidden'}`
        }}
        onClick={e => {
            e.stopPropagation();
            e.preventDefault()
        }}
        >
            <div className="picker-heading">
                <h3>Chỉnh sửa text</h3>
            </div>
            <div className="row mb-12">
                <div className="col col-9">
                    <DropDown typeDrop="Font" title="Font chữ" content={font} getVal={getVal}/>
                </div>
                <div className="col col-3 dropdown-numb">
                    <DropDown typeDrop="FontSize" title="12" content={fontSize} getVal={getVal}/>
                </div>
            </div>
            <div className="row mb-12">
                <div className="col col-6">
                    <div className="d-flex align-content dropdown-numb">
                        <p className="no-wrap mr-6">Giãn dòng</p>
                        <DropDown typeDrop="LineHeight" title="12" content={fontSize} getVal={getVal}/>
                    </div>
                </div>
                <div className="col col-6">
                    <div className="d-flex align-content dropdown-numb">
                        <p className="no-wrap mr-6">Giãn chữ</p>
                        <DropDown typeDrop="LetterSpacing" title="12" content={fontSize} getVal={getVal}/>
                    </div>
                </div>
            </div>

            <div className="row mb-12">
                <div className="col coll-7">
                    <div className="d-flex align-content">
                        <p className="no-wrap mr-6">Kiểu chữ</p>
                        <div className="group-btn">
                            <button type="button" name="" id="" className={ textContext.FontWeight ? "btn btn-square active" : "btn btn-square" } onClick={() => textContext.setFontWeight(!textContext.FontWeight)}>
                                <svg xmlns="http://www.w3.org/2000/svg" width={9} height={12} viewBox="0 0 9 12" fill="none">
                                    <path d="M0 12V0H4.24214C5.71165 0 6.82625 0.28022 7.58595 0.840659C8.34566 1.3956 8.72551 2.21154 8.72551 3.28846C8.72551 3.87637 8.57301 4.3956 8.26802 4.84615C7.96303 5.29121 7.53882 5.61813 6.99538 5.82692C7.61645 5.98077 8.10444 6.29121 8.45933 6.75824C8.81978 7.22528 9 7.7967 9 8.47253C9 9.62637 8.62847 10.5 7.8854 11.0934C7.14233 11.6868 6.08318 11.989 4.70795 12H0ZM2.49538 6.77473V10.0137H4.63309C5.22089 10.0137 5.67837 9.87637 6.00555 9.60165C6.33826 9.32143 6.50462 8.93681 6.50462 8.4478C6.50462 7.3489 5.93068 6.79121 4.78281 6.77473H2.49538ZM2.49538 5.02747H4.34196C5.60074 5.00549 6.23013 4.50824 6.23013 3.53571C6.23013 2.99176 6.06932 2.60165 5.74769 2.36538C5.43161 2.12363 4.92976 2.00275 4.24214 2.00275H2.49538V5.02747Z" fill="#999999" />
                                </svg>
                            </button>
                            <button type="button" name="" id="" className={textContext.FontStyle ? "btn btn-square active" : "btn btn-square"} onClick={() => textContext.setFontStyle(!textContext.FontStyle)}>
                                <svg xmlns="http://www.w3.org/2000/svg" width={9} height={13} viewBox="0 0 9 13" fill="none">
                                    <path d="M4.62799 11.25H2.53564L4.33521 0.852539H6.42755L4.62799 11.25Z" fill="#999999" />
                                    <path fillRule="evenodd" clipRule="evenodd" d="M2.25 0.5625C2.25 0.25184 2.34299 0 2.45769 0H8.79231C8.90701 0 9 0.25184 9 0.5625C9 0.87316 8.90701 1.125 8.79231 1.125H2.45769C2.34299 1.125 2.25 0.87316 2.25 0.5625Z" fill="#999999" />
                                    <path fillRule="evenodd" clipRule="evenodd" d="M0 11.8125C0 11.5018 0.092987 11.25 0.207692 11.25H6.54231C6.65701 11.25 6.75 11.5018 6.75 11.8125C6.75 12.1232 6.65701 12.375 6.54231 12.375H0.207692C0.092987 12.375 0 12.1232 0 11.8125Z" fill="#999999" />
                                </svg>
                            </button>
                            <button type="button" name="" id="" className={textContext.Underline ? "btn btn-square active" : "btn btn-square"} onClick={() => textContext.setUnderline(!textContext.Underline)}>
                                <svg xmlns="http://www.w3.org/2000/svg" width={13} height={12} viewBox="0 0 13 12" fill="none">
                                    <path d="M9.55665 0V5.71246C9.55665 6.66155 9.25882 7.41209 8.66315 7.96408C8.07145 8.51607 7.26134 8.79206 6.23282 8.79206C5.22019 8.79206 4.41604 8.52401 3.82037 7.98791C3.2247 7.45181 2.92091 6.71516 2.909 5.77798V0H4.696V5.72437C4.696 6.29224 4.83102 6.70722 5.10105 6.96931C5.37506 7.22744 5.75232 7.3565 6.23282 7.3565C7.23752 7.3565 7.74781 6.82834 7.76369 5.77202V0H9.55665Z" fill="#999999" />
                                    <path fillRule="evenodd" clipRule="evenodd" d="M0 11.4455C0 11.1393 0.168055 10.891 0.375362 10.891H11.8239C12.0312 10.891 12.1993 11.1393 12.1993 11.4455C12.1993 11.7518 12.0312 12 11.8239 12H0.375362C0.168055 12 0 11.7518 0 11.4455Z" fill="#999999" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                <div className="col coll-5">
                    <div className="d-flex align-content">
                    <div className="group-btn">
                            <button type="button" name="" id="" className={ textContext.Align === 'left' ? 'btn btn-square active' : 'btn btn-square' } onClick={() => textContext.setAlign('left')}>
                                <svg xmlns="http://www.w3.org/2000/svg" width={13} height={12} viewBox="0 0 13 12" fill="none">
                                    <path fillRule="evenodd" clipRule="evenodd" d="M0 0.6C0 0.268629 0.179086 0 0.4 0H12.6C12.8209 0 13 0.268629 13 0.6C13 0.931371 12.8209 1.2 12.6 1.2H0.4C0.179086 1.2 0 0.931371 0 0.6Z" fill="#999999" />
                                    <path fillRule="evenodd" clipRule="evenodd" d="M0 5.6C0 5.26863 0.110207 5 0.246154 5H7.75385C7.88979 5 8 5.26863 8 5.6C8 5.93137 7.88979 6.2 7.75385 6.2H0.246154C0.110207 6.2 0 5.93137 0 5.6Z" fill="#999999" />
                                    <path fillRule="evenodd" clipRule="evenodd" d="M0 10.6C0 10.2686 0.151534 10 0.338462 10H10.6615C10.8485 10 11 10.2686 11 10.6C11 10.9314 10.8485 11.2 10.6615 11.2H0.338462C0.151534 11.2 0 10.9314 0 10.6Z" fill="#999999" />
                                </svg>
                            </button>
                            <button type="button" name="" id="" className={ textContext.Align === 'center' ? 'btn btn-square active' : 'btn btn-square' } onClick={() => textContext.setAlign('center')}>
                                <svg xmlns="http://www.w3.org/2000/svg" width={13} height={12} viewBox="0 0 13 12" fill="none">
                                    <path fillRule="evenodd" clipRule="evenodd" d="M0 0.6C0 0.268629 0.179086 0 0.4 0H12.6C12.8209 0 13 0.268629 13 0.6C13 0.931371 12.8209 1.2 12.6 1.2H0.4C0.179086 1.2 0 0.931371 0 0.6Z" fill="#999999" />
                                    <path fillRule="evenodd" clipRule="evenodd" d="M3 5.6C3 5.26863 3.11021 5 3.24615 5H10.7538C10.8898 5 11 5.26863 11 5.6C11 5.93137 10.8898 6.2 10.7538 6.2H3.24615C3.11021 6.2 3 5.93137 3 5.6Z" fill="#999999" />
                                    <path fillRule="evenodd" clipRule="evenodd" d="M1 10.6C1 10.2686 1.15153 10 1.33846 10H11.6615C11.8485 10 12 10.2686 12 10.6C12 10.9314 11.8485 11.2 11.6615 11.2H1.33846C1.15153 11.2 1 10.9314 1 10.6Z" fill="#999999" />
                                </svg>
                            </button>
                            <button type="button" name="" id="" className={ textContext.Align === 'right' ? 'btn btn-square active' : 'btn btn-square' } onClick={() => textContext.setAlign('right')}>
                                <svg xmlns="http://www.w3.org/2000/svg" width={13} height={12} viewBox="0 0 13 12" fill="none">
                                    <path fillRule="evenodd" clipRule="evenodd" d="M0 0.6C0 0.268629 0.179086 0 0.4 0H12.6C12.8209 0 13 0.268629 13 0.6C13 0.931371 12.8209 1.2 12.6 1.2H0.4C0.179086 1.2 0 0.931371 0 0.6Z" fill="#999999" />
                                    <path fillRule="evenodd" clipRule="evenodd" d="M5 5.6C5 5.26863 5.11021 5 5.24615 5H12.7538C12.8898 5 13 5.26863 13 5.6C13 5.93137 12.8898 6.2 12.7538 6.2H5.24615C5.11021 6.2 5 5.93137 5 5.6Z" fill="#999999" />
                                    <path fillRule="evenodd" clipRule="evenodd" d="M2 10.6C2 10.2686 2.15153 10 2.33846 10H12.6615C12.8485 10 13 10.2686 13 10.6C13 10.9314 12.8485 11.2 12.6615 11.2H2.33846C2.15153 11.2 2 10.9314 2 10.6Z" fill="#999999" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row mb-12 align-content">
                <div className="col coll-5">
                    <div className="d-flex align-content">
                        <p className="no-wrap mr-6">Màu chữ</p>
                        <button type="button" name="" id="" className="btn btn-md btn-color" style={{background: '#EB5757'}} onClick={() => setShowColorText(!ShowColorText)}> </button>
                        {ShowColorText ? <ChangeColorEx type="colorTextEdit" /> : ""}
                    </div>
                </div>
                <div className="col coll-7">
                    <div className="d-flex align-content dropdown-numb">
                        <p className="no-wrap mr-6">Viền chữ</p>
                        <DropDown typeDrop="BTS" title="12" content={fontSize} getVal={getVal}/>
                        <button 
                            type="button" 
                            className="btn btn-sm btn-color ml-6" 
                            style={{background: '#FDD24B'}}
                            onClick={() => setShowColorRimText(!ShowColorRimText)}
                        > </button>
                        {ShowColorRimText ? <ChangeColorEx type="colorTextRimEdit" /> : ""}
                    </div>
                </div>
            </div>

            <div className="br-tag"></div>

            <div className="row align-content">
                <div className="col mb-12">
                    <div className="d-flex align-content">
                        <p className="no-wrap mr-6">Màu khung chữ</p>
                        <button type="button" name="" id="" className="btn btn-md btn-color" style={{background: '#FFFFFF'}} onClick={() => setShowColorBorder(!ShowColorBorder)}> </button>
                        {ShowColorBorder ? <ChangeColorEx type="colorBorderEdit" /> : ""}
                    </div>
                </div>
                <div className="col">
                    <div className="d-flex align-content dropdown-numb">
                        <p className="no-wrap mr-6">Viền khung</p>
                        <DropDown title="12" content={fontSize} />
                        <button type="button" name="" id="" className="btn btn-sm btn-color ml-6" style={{background: '#219653'}} onClick={() => setShowColorRimBorder(!ShowColorRimBorder)}> </button>
                        {ShowColorRimBorder ? <ChangeColorEx type="colorBorderRimEdit" /> : ""}
                    </div>
                </div>
            </div>
            
            <div className="br-tag mb-12"></div>
            
            <div className="row align-content">
                <div className="col mb-12">
                    <div className="d-flex align-content">
                        <p className="no-wrap mr-6">Màu đổ bóng</p>
                        <button type="button" name="" id="" className="btn btn-md btn-color" style={{background: '#474747'}}> </button>
                    </div>
                </div>
            </div>
            <div className="row align-content">
                <div className="col col-3 mb-12">
                    <div className="d-flex align-content">
                        <p className="no-wrap mr-6">Bóng mờ</p>
                    </div>
                </div>
                <div className="col col-9 mb-12">
                    <div className="d-flex align-content dropdown-numb">
                            <InputRange
                                maxValue={100}
                                minValue={0}
                                value={shadow}
                                onChange={value => {
                                    setShadow(value)
                                    textContext.setShadowBlur(value)
                                }} 
                            />
                            <p className="value-range">{ shadow }px</p>
                    </div>
                </div>
            </div>
            <div className="row align-content">
                <div className="col col-3 mb-12">
                    <div className="d-flex align-content">
                        <p className="no-wrap mr-6">Ngang</p>
                    </div>
                </div>
                <div className="col col-9 mb-12">
                    <div className="d-flex align-content dropdown-numb">
                            <InputRange
                                maxValue={100}
                                minValue={0}
                                value={hoz}
                                onChange={value => {
                                    setHoz(value)
                                    textContext.setShadowHoz(value)
                                }} 
                            />
                            <p className="value-range">{ hoz }px</p>
                    </div>
                </div>
            </div>
            <div className="row align-content">
                <div className="col col-3 mb-12">
                    <div className="d-flex align-content">
                        <p className="no-wrap mr-6">Dọc</p>
                    </div>
                </div>
                <div className="col col-9 mb-12">
                    <div className="d-flex align-content dropdown-numb">
                            <InputRange
                                maxValue={100}
                                minValue={0}
                                value={vel}
                                onChange={value => {
                                    setVel(value)
                                    textContext.setShadowVel(value)
                                }} 
                            />
                            <p className="value-range">{ vel }px</p>
                    </div>
                </div>
            </div>
            
        </div>
    )
}
