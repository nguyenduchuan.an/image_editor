import React, { useContext } from 'react'
import { HeaderMain } from './HeaderMain/HeaderMain';
import { SideLeft } from './SideLeft/SideLeft';
import { SideRight } from './SideRight/SideRight';
import { BodyMain } from './BodyMain/BodyMain';
import { ImageContext } from '../Contexts/ImageContext';
export const MainApp = () => {
    const imageContext = useContext(ImageContext)
    return (
        <div id="App" className={ !imageContext.SideRight ? 'App sr' : 'App'}>
            <HeaderMain />
            <SideLeft />
            <SideRight />
            <BodyMain />
        </div>
    )
}
