import React, { useState, useContext } from 'react'
import InputRange from 'react-input-range'
import 'react-input-range/lib/css/index.css'
import './SideRight.scss'
import item from './../../assets/images/frame-item.png'
import { ImageContext } from '../../Contexts/ImageContext'

const listFrame = [
    {
        name: 'frame1',
        grid: [
            {i: 'a', w: 12, h: 6, t: 0, l: 0},
            {i: 'b', w: 12, h: 6, t: 6, l: 0}
        ]
    },
    {
        name: 'frame2',
        grid: [
            {i: 'a', w: 6, h: 12, t: 0, l: 0},
            {i: 'b', w: 6, h: 12, t: 0, l: 6}
        ]
    },
    {
        name: 'frame3',
        grid: [
            {i: 'a', w: 12, h: 4, t: 0, l: 0},
            {i: 'b', w: 12, h: 8, t: 4, l: 0}
        ]
    },
    {
        name: 'frame4',
        grid: [
            {i: 'a', w: 12, h: 8, t: 0, l: 0},
            {i: 'b', w: 12, h: 4, t: 8, l: 0}
        ]
    },
    {
        name: 'frame5',
        grid: [
            {i: 'a', w: 12, h: 3, t: 0, l: 0},
            {i: 'b', w: 12, h: 9, t: 3, l: 0}
        ]
    },
    {
        name: 'frame6',
        grid: [
            {i: 'a', w: 12, h: 9, t: 0, l: 0},
            {i: 'b', w: 12, h: 3, t: 9, l: 0}
        ]
    },
    {
        name: 'frame7',
        grid: [
            {i: 'a', w: 4, h: 12, t: 0, l: 0},
            {i: 'b', w: 8, h: 12, t: 0, l: 4}
        ]
    },
    {
        name: 'frame8',
        grid: [
            {i: 'a', w: 8, h: 12, t: 0, l: 0},
            {i: 'b', w: 4, h: 12, t: 0, l: 8}
        ]
    },
    {
        name: 'frame9',
        grid: [
            {i: 'a', w: 3, h: 12, t: 0, l: 0},
            {i: 'b', w: 9, h: 12, t: 0, l: 3}
        ]
    },
    {
        name: 'frame10',
        grid: [
            {i: 'a', w: 9, h: 12, t: 0, l: 0},
            {i: 'b', w: 3, h: 12, t: 0, l: 9}
        ]
    },
    {
        name: 'frame11',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 1%, 0% 100%, 99% 100%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(1% 0, 100% 0, 100% 99%)'}
        ]
    },
    {
        name: 'frame12',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 0 99%, 99% 0)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(100% 100%, 1% 100%, 100% 1%)'}
        ]
    },
    {
        name: 'frame14',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 100% 0, 100% 59%, 0 39%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 41%, 100% 61%, 100% 100%, 0 100%)'}
        ]
    },
    {
        name: 'frame15',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 100% 0, 100% 39%, 0 59%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 61%, 100% 41%, 100% 100%, 0 100%)'}
        ]
    },
    {
        name: 'frame16',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 59% 0, 39% 100%, 0 100%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(61% 0, 100% 0, 100% 100%, 41% 100%)'}
        ]
    },
    {
        name: 'frame17',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 39% 0, 59% 100%, 0 100%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(41% 0, 100% 0, 100% 100%, 61% 100%)'}
        ]
    },
    {
        name: 'frame18',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 100% 0, 100% 54%, 0 44%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 46%, 100% 56%, 100% 100%, 0 100%)'}
        ]
    },
    {
        name: 'frame19',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 100% 0, 100% 44%, 0 54%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 56%, 100% 46%, 100% 100%, 0 100%)'}
        ]
    },
    {
        name: 'frame20',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 54% 0, 44% 100%, 0 100%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(56% 0, 100% 0, 100% 100%, 46% 100%)'}
        ]
    },
    {
        name: 'frame21',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 44% 0, 54% 100%, 0 100%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(46% 0, 100% 0, 100% 100%, 56% 100%)'}
        ]
    },
    {
        name: 'frame13',
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0}
        ]
    },
    {
        name: 'frame22',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 4, t: 0, l: 0},
            {i: 'b', w: 12, h: 4, t: 4, l: 0},
            {i: 'c', w: 12, h: 4, t: 8, l: 0, text: 'add text'}
        ]
    },
    {
        name: 'frame23',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 4, t: 0, l: 0, text: 'add text'},
            {i: 'b', w: 12, h: 4, t: 4, l: 0},
            {i: 'c', w: 12, h: 4, t: 8, l: 0}
        ]
    },
    {
        name: 'frame24',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 4, t: 0, l: 0, text: 'add text'},
            {i: 'b', w: 12, h: 4, t: 4, l: 0},
            {i: 'c', w: 12, h: 4, t: 8, l: 0}
        ]
    },
    {
        name: 'frame25',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 4, t: 0, l: 0, text: 'add text'},
            {i: 'b', w: 12, h: 4, t: 4, l: 0},
            {i: 'c', w: 12, h: 4, t: 8, l: 0}
        ]
    },
    {
        name: 'frame26',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 8, t: 0, l: 0},
            {i: 'b', w: 12, h: 4, t: 8, l: 0}
        ]
    },
    {
        name: 'frame27',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 3, t: 0, l: 0},
            {i: 'b', w: 12, h: 9, t: 3, l: 0}
        ]
    },
    {
        name: 'frame28',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 59% 0, 39% 100%, 0 100%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(61% 0, 100% 0, 100% 100%, 41% 100%)'}
        ]
    },
    {
        name: 'frame29',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(0 0, 39% 0, 59% 100%, 0 100%)'},
            {i: 'b', w: 12, h: 12, t: 0, l: 0, cp: 'polygon(41% 0, 100% 0, 100% 100%, 61% 100%)'}
        ]
    },
    {
        name: 'frame30',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 9, t: 0, l: 0},
            {i: 'b', w: 12, h: 3, t: 9, l: 0},
            {i: 'c', w: 10, h: 4, t: 7, l: 1}
        ]
    },
    {
        name: 'frame31',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 3, t: 0, l: 0},
            {i: 'b', w: 12, h: 9, t: 3, l: 0},
            {i: 'c', w: 10, h: 4, t: 1, l: 1}
        ]
    },
    {
        name: 'frame32',
        default: true,
        grid: [
            {i: 'a', w: 12, h: 6, t: 0, l: 0},
            {i: 'b', w: 12, h: 6, t: 6, l: 0}
        ]
    },
    {
        name: 'frame33',
        default: true,
        grid: [
            {i: 'a', w: 6, h: 12, t: 0, l: 0},
            {i: 'b', w: 6, h: 12, t: 0, l: 6}
        ]
    },
    {
        name: 'frame34',
        default: true,
        grid: [
            {i: 'a', w: 7, h: 10, t: 1, l: 0},
            {i: 'b', w: 7, h: 12, t: 0, l: 5}
        ]
    },
    {
        name: 'frame35',
        default: true,
        grid: [
           {i: 'a', w: 7, h: 10, t: 1, l: 5},
            {i: 'b', w: 7, h: 12, t: 0, l: 0}
        ]
    },
    {
        name: 'frame36',
        default: true,
        grid: [
            {i: 'a', w: 6, h: 8, t: 1, l: 1},
            {i: 'b', w: 6, h: 8, t: 3, l: 5},
            {i: 'c', w: 8, h: 1, t: 10, l: 0}
        ]
    },
    {
        name: 'frame37',
        default: true,
        grid: [
            {i: 'a', w: 5.5, h: 9, t: 0.5, l: 2},
            {i: 'b', w: 5.5, h: 9, t: 1, l: 5},
            {i: 'c', w: 10, h: 1, t: 10, l: 1}
        ]
    },
    {
        name: 'frame38',
        default: true,
        grid: [
            {i: 'a', w: 10, h: 7, t: 0.5, l: 0.5},
            {i: 'b', w: 10, h: 5, t: 6.5, l: 1.5}
        ]
    },
    {
        name: 'frame39',
        default: true,
        grid: [
            {i: 'a', w: 7, h: 12, t: 0, l: 0},
            {i: 'b', w: 7, h: 12, t: 0, l: 5.5},
            {i: 'c', w: 7, h: 1, t: 10.5, l: 4.5}
        ]
    },
]

export const SideRight = () => {
    const [activeFrame, setActiveFrame] = useState(0)
    const imageContext = useContext(ImageContext)
    const radius = imageContext.Radius
    const distance = imageContext.Padding
    return (
        <div className="side-right">
            <div className="content-side">
                <h3 className="title-frame">
                    Frame
                </h3>
                <div className="list-frame">
                    {
                        listFrame.map((image, key) => {
                            
                            return <div key={key} className={ key === activeFrame ? "frame-item active" : "frame-item" }>
                                <div data-content={image} onClick={ (e) => {
                                        setActiveFrame(key)
                                        imageContext.setLayout(image.grid)
                                        imageContext.setDefaultLayout(image)
                                    } 
                                } className={'frame-item__content ' + image.name }></div>
                            </div>
                        })
                    }
                    
                </div>
            </div>
            <div className="control-side">
                <div className="control-item">
                    <h5>Khoảng cách</h5>
                    <div className="d-flex">
                        <InputRange
                            maxValue={100}
                            minValue={0}
                            value={distance}
                            onChange={value => {
                                imageContext.setPadding(value)
                            }} 
                        />
                        
                        <span>{distance}px</span>
                    </div>
                    
                </div>
                <div className="control-item">
                    <h5>Độ bo góc</h5>
                    <div className="d-flex">
                        <InputRange
                            maxValue={150}
                            minValue={0}
                            value={radius}
                            onChange={value => {
                                imageContext.setRadius(value)
                            }} 
                        />
                        <span>{ radius }%</span>
                    </div>
                </div>
            </div>
            <button 
                type="button"
                className="btn control-sidebar" 
                onClick={() => imageContext.setSideRight(!imageContext.SideRight)}
            >
                <svg xmlns="http://www.w3.org/2000/svg" width={6} height={12} viewBox="0 0 6 12" fill="none">
                    <path d="M1 1L5 6.08207L1 11" stroke="#222222" strokeWidth="1.3" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
            </button>
        </div>
    )
}
