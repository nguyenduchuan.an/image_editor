import React, { useState, useContext } from 'react';
import { ColorPicker } from 'react-color-gradient-picker';
import 'react-color-gradient-picker/dist/index.css';
import './ColorPicker.scss'
import { ImageContext } from '../../Contexts/ImageContext';
import { TextContext } from '../../Contexts/TextContext';

const color = {
    red: 255,
    green: 0,
    blue: 0,
    alpha: 1,
};

const popover = {
    position: 'absolute',
    left: '100%',
    zIndex: '2',
}
const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
    background: 'rgba($color: #000000, $alpha: 1.0)'
}

export const ChangeColor = (props) => {
    const [display, setDisplay] = useState(false)
    const [colorAttrs, setColorAttrs] = useState(color);
    const imageContext = useContext(ImageContext)
    const textContext = useContext(TextContext)
    const onChange = (colorAttrs) => {
        setColorAttrs(colorAttrs);
        if(props.type){
            switch (props.type) {
                case 'colorMain':
                    imageContext.addBackGround(colorAttrs.style)
                    break;

                default:
                    break;
            }
        }
    };
    return (
        <div className="color-item add-color-new">
            <a onClick={ () => setDisplay(!display) } href="#" className="d-flex">
                <svg xmlns="http://www.w3.org/2000/svg" width={12} height={12} viewBox="0 0 12 12" fill="none">
                    <path d="M6 1V11" stroke="#888888" strokeWidth="1.2" strokeLinecap="round" strokeLinejoin="round" />
                    <path d="M1 6H11" stroke="#888888" strokeWidth="1.2" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
            </a>
            { display ? <div style={ popover }>
            <div style={ cover } onClick={ () => setDisplay(false) }/>
                <ColorPicker
                onStartChange={onChange}
                onChange={onChange}
                onEndChange={onChange}
                color={colorAttrs}
            />
            </div> : null }
        </div>
    )
}
