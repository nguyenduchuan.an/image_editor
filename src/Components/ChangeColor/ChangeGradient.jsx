import React, { useState, useContext } from 'react';
import { ColorPicker } from 'react-color-gradient-picker';
import 'react-color-gradient-picker/dist/index.css';
import { ImageContext } from '../../Contexts/ImageContext';

const gradient = {
    points: [
        {
            left: 0,
            red: 0,
            green: 0,
            blue: 0,
            alpha: 1,
        },
        {
            left: 100,
            red: 255,
            green: 0,
            blue: 0,
            alpha: 1,
        },
    ],
    degree: 0,
    type: 'linear',
};

const popover = {
    position: 'absolute',
    left: '100%',
    zIndex: '2',
}
const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
}

export const ChangeGradient = () => {
    const [gradientAttrs, setGradientAttrs] = useState(gradient);
    const [display, setDisplay] = useState(false)
    const imageContext = useContext(ImageContext)
    const onChange = (gradientAttrs) => {
        setGradientAttrs(gradientAttrs);
    };
    return (
        <div className="color-item add-color-new">
        <a onClick={ () => setDisplay(!display) } href="#" className="d-flex">
            <svg xmlns="http://www.w3.org/2000/svg" width={12} height={12} viewBox="0 0 12 12" fill="none">
                <path d="M6 1V11" stroke="#888888" strokeWidth="1.2" strokeLinecap="round" strokeLinejoin="round" />
                <path d="M1 6H11" stroke="#888888" strokeWidth="1.2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </a>
        { display ? <div style={ popover }>
        <div style={ cover } onClick={ () => setDisplay(false) }/>
        <ColorPicker
            onStartChange={onChange}
            onChange={imageContext.addBackGround(gradientAttrs.style)}
            onEndChange={onChange}
            gradient={gradientAttrs}
            isGradient
        />
        </div> : null }
    </div>
        
    )
}
