import React, { useState, useContext } from 'react';
import { ColorPicker } from 'react-color-gradient-picker';
import 'react-color-gradient-picker/dist/index.css';
import './ColorPicker.scss'
import { ImageContext } from '../../Contexts/ImageContext';
import { TextContext } from '../../Contexts/TextContext';

const color = {
    red: 255,
    green: 255,
    blue: 255,
    alpha: 1,
};

const popover = {
    position: 'absolute',
    left: '100%',
    zIndex: '2',
}
const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
}

export const ChangeColorEx = (props) => {
    const [display, setDisplay] = useState(true)
    const [colorAttrs, setColorAttrs] = useState(color);
    const imageContext = useContext(ImageContext)
    const textContext = useContext(TextContext)

    const onChange = (colorAttrs) => {
        setColorAttrs(colorAttrs);
        if(props.type){
            switch (props.type) {
                case 'colorTextEdit':
                    textContext.setFontColor(colorAttrs.style)
                    break;
                    
                case 'colorTextRimEdit':
                    // textContext.setFontColor(colorAttrs.style)
                    break;
                    
                case 'colorBorderEdit':
                    // textContext.setFontColor(colorAttrs.style)
                    break;
                    
                case 'colorBorderRimEdit':
                    // textContext.setFontColor(colorAttrs.style)
                    break;
                    
                case 'colorItem':
                    imageContext.addSolidColorItem(colorAttrs.style)
                    break;
                
                default:
                    break;
            }
        }
    };
    return (
        <div>
            { display ? <div style={ popover }>
            <div style={ cover } onClick={ () => {
                setDisplay(false)
                imageContext.setShowChange(false)
                imageContext.setIsChange(false)
            } }/>
            <ColorPicker
                onStartChange={onChange}
                onChange={onChange}
                onEndChange={onChange}
                color={colorAttrs}
            />
            </div> : null }
        </div>
    )
}
