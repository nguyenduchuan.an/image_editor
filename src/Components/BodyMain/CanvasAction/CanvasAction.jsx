import React from 'react'

export const CanvasAction = () => {
    return (
        <div className="action-content">
            <div className="action-item">
                <a href="#">Thêm ảnh</a>
            </div>
            <div className="action-item">
                <a href="#">Đổ màu</a>
            </div>
        </div>
    )
}
