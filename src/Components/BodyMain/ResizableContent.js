import React, { Fragment, useState, useContext } from 'react'
import ResizableRect from 'react-resizable-rotatable-draggable'
import { TextEditor } from '../TextEditor.js/TextEditor'
import { TextProvider, TextContext } from '../../Contexts/TextContext'
import { ImageEditor } from '../ImageEditor/ImageEditor'


const ResizableContent = props => {
    const [width, setWidth] = useState(props.width)
    const [height, setHeight] = useState(props.height)
    const [top, setTop] = useState(props.top)
    const [left, setLeft] = useState(props.left)
    const [rotateAngle, setRotateAngle] = useState(props.rotateAngle)
    const [showEditor, setshowEditor] = useState(true)
    const [FontSize, setFontSize] = useState(12)
    const [LtSpacing, setLtSpacing] = useState(1)
    const [LineHight, setLineHight] = useState(12)
    const [Align, setAlign] = useState('flex-start')
    
    const textContext = useContext(TextContext);
    const contentStyle = {
      top,
      left,
      width,
      height,
      position: 'absolute',
      transform: `rotate(${rotateAngle}deg)`,
      // alignItems: 'center',
      // display: 'flex',
      fontFamily: `${textContext.Font}`,
      textAlign: `${textContext.Align}`,
      color: textContext.FontColor,
      fontSize: `${textContext.FontSize}px`,
      fontStyle: `${textContext.FontStyle ? 'italic' : 'normal'}`,
      lineHeight: `${textContext.LineHeight}px`,
      letterSpacing: `${textContext.LetterSpacing}px`,
      textShadow: `${textContext.ShadowHoz}px ${textContext.ShadowVel}px ${textContext.ShadowBlur}px black`,
      fontWeight: `${textContext.FontWeight ? 'bold' : 'normal'}`,
      textDecoration: `${textContext.Underline? 'underline': 'none'}`
    }

  const handleResize = (style, isShiftKey, type) => {
    const { top, left, width, height } = style
    setWidth(Math.round(width))
    setHeight(Math.round(height))
    setTop(Math.round(top))
    setLeft(Math.round(left))
  }

  const handleRotate = rotateAngle => {
    setRotateAngle(rotateAngle)
  }

  const handleDrag = (deltaX, deltaY) => {
    setLeft(left + deltaX)
    setTop(top + deltaY)
    setshowEditor(false)
  }

  return (
    <Fragment>
      <div style={contentStyle}>{props.children}</div>
        <ResizableRect
          top={top}
          rotatable
          left={left}
          // aspectRatio
          minWidth={10}
          width={width}
          minHeight={10}
          height={height}
          onDrag={handleDrag}
          onRotate={handleRotate}
          onResize={handleResize}
          zoomable="n, w, s, e, nw, ne, se, sw"
          rotateAngle={rotateAngle}
          onClick={() => setshowEditor(true)}
        />
        {
          props.isText ? <TextEditor top={top} left={left + width +10} show={showEditor}  /> : <ImageEditor top={top} left={left + width +10}/>
        }
        
    </Fragment>
  )
}

export default ResizableContent
