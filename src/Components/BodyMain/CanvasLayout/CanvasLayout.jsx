import React, {useRef, useState, useEffect} from 'react'
import { fabric } from "fabric"

export const CanvasLayout = (props) => {
    const [height, setHeight] = useState(null)
    const ref = useRef(null);

    useEffect(() => {
        if(ref.current){
            setHeight(ref.current.offsetHeight)
        }
    }, [ref.current]);

    const drawC = (e, color) => {
        var canvas = new fabric.Canvas(`${e.target.id}`);
        console.log(color);
        var rect = new fabric.Rect({
            top : 0,
            left : 0,
            width : props.area,
            height : props.area/2,
            fill : `${color}`
        });
    
        canvas.add(rect);
    }
    return (
        <div key={props.item.i}>
            {
                console.log(props.item.i)
            }
            <canvas 
                ref={ref}
                id="canvas1"
                width={props.area - props.Spacing*2}
                height={ height }
                className="box-udf" 
                onClick={(e) => drawC(e, "black") }
            >
            </canvas>
        </div>
    )
}
