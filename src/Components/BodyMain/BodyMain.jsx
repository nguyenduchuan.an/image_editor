import React, { useEffect, useRef, useState, useContext, Fragment } from 'react'

import './BodyMain.scss'
import iconClose from './../../assets/icon/icon-close.png'
import { ImageEditor } from '../ImageEditor/ImageEditor'
import { ImageContext } from '../../Contexts/ImageContext';
import { ChangeColorEx } from '../ChangeColor/ChangeColorEx';
import ResizableContent from './ResizableContent';
import { TextProvider } from '../../Contexts/TextContext'


export const BodyMain = () => {

    // The next features we’d like to add are the Clean and Undo
    const ref = useRef(null);
    const [height, setHeight] = useState(null)
    // get data ImageContext
    const imageContext = useContext(ImageContext);
    const td = imageContext.td;
    const area = imageContext.area;
    const square = area / 12;
    const padding = imageContext.Padding;
    const radius = imageContext.Radius;
    const scale = imageContext.Padding / 100;
    const indexMain = imageContext.Index;
    const bgMain = imageContext.Background;



    useEffect(() => {
        if (ref.current) {
            setHeight(ref.current.offsetHeight)
        }
    }, [ref.current]);

    const checkWidth = (item) => {
        if (item.w === 12 && item.h === 12) {
            return item.w * square - 2 * padding
        } else if (item.w === 12 & item.h < 12) {
            return item.w * square - 2 * padding
        } else {
            return item.w * square - ((imageContext.Layout.length + 1) * padding) / 2
        }
    }
    const checkHeight = (item) => {
        if (item.w === 12 && item.h === 12) {
            return item.h * square - 2 * padding
        } else if (item.h === 12 & item.w < 12) {
            return item.h * square - 2 * padding
        } else {
            return item.h * square - ((imageContext.Layout.length + 1) * padding) / 2
        }
    }
    return (
        <div className="main">
            <div className="main-content">
                <div className="view-wp">
                    <div className={imageContext.DefaultLayout.default === true ? `canvas-section ${imageContext.DefaultLayout.name}` : 'canvas-section'} style={{
                        position: 'relative',
                        width: `${area}px`,
                        height: `${area}px`,
                        background: `${bgMain}`,
                    }}>
                        {
                            imageContext.Layout.map((item, index) => {
                                return (
                                    <div
                                        draggable="true"
                                        key={index}
                                        id={item.i}
                                        data-edit={false}
                                        onDrag={(e) => {
                                            console.log(e)
                                        }}
                                        onClick={(e) => {
                                            if(e.target.dataset.edit == 'false'){
                                                imageContext.showTypeElm(e)
                                            }else{
                                                imageContext.showEditImage(e)
                                            }
                                            
                                        }}
                                        className="item"
                                        style={{
                                            position: 'absolute',
                                            width: `${checkWidth(item)}px`,
                                            height: `${checkHeight(item)}px`,
                                            top: `${item.t === 0 ? (item.t + padding) : item.t * square + padding / 2}px`,
                                            left: `${item.l === 0 ? (item.l + padding) : item.l * square + padding / 2}px`,
                                            clipPath: `${item.cp ? item.cp : ''}`,
                                            borderRadius: `${radius}px`,
                                            background: `url("data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='${ radius }' ry='${ radius }' stroke='%23F5A623FF' stroke-width='2' stroke-dasharray='11' stroke-dashoffset='0' stroke-linecap='square'/%3e%3c/svg%3e")`
                                        }}
                                    >
                                        {/* <div className="child-item a" onClick={e => e.stopPropagation()}></div> */}
                                        <img 
                                            className="del-elm"
                                            onClick={e => {
                                                e.stopPropagation();
                                                imageContext.removeItem(e.target.parentElement)
                                            }}
                                            src={iconClose} 
                                            alt=""
                                        />
                                        {/* <svg 
                                            className="del-elm"
                                            onClick={e => {
                                                e.stopPropagation();
                                                imageContext.removeItem(e.target.parentElement)
                                            }}
                                            xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 409.806 409.806" style={{enableBackground: 'new 0 0 409.806 409.806'}} xmlSpace="preserve">
                                            <g>
                                            <g>
                                                <path d="M228.929,205.01L404.596,29.343c6.78-6.548,6.968-17.352,0.42-24.132c-6.548-6.78-17.352-6.968-24.132-0.42    c-0.142,0.137-0.282,0.277-0.42,0.42L204.796,180.878L29.129,5.21c-6.78-6.548-17.584-6.36-24.132,0.42    c-6.388,6.614-6.388,17.099,0,23.713L180.664,205.01L4.997,380.677c-6.663,6.664-6.663,17.468,0,24.132    c6.664,6.662,17.468,6.662,24.132,0l175.667-175.667l175.667,175.667c6.78,6.548,17.584,6.36,24.132-0.42    c6.387-6.614,6.387-17.099,0-23.712L228.929,205.01z" />
                                            </g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                        </svg> */}
                                    
                                    </div>
                                )
                            })
                        }
                        <Fragment>
                            {
                                imageContext.elmText.map((item, key) => {
                                    return <TextProvider key={key}>
                                        <ResizableContent
                                            isText={item.isText}
                                            top={100}
                                            left={100}
                                            width={150}
                                            height={30}
                                            rotateAngle={0}
                                        >
                                            <div className='vcedit-text'>
                                                {item.content}
                                            </div>
                                        </ResizableContent>
                                    </TextProvider>
                                })
                            }
                        </Fragment>
                        <Fragment>
                            {
                                imageContext.elmImage.map((item, key) => {
                                    return <TextProvider key={key}>
                                        <ResizableContent
                                            isText={item.isText}
                                            top={100}
                                            left={100}
                                            width={100}
                                            height={100}
                                            rotateAngle={0}
                                        >
                                            <img className="vcedit-icon" src={item.url} alt="" />
                                        </ResizableContent>
                                    </TextProvider>
                                })
                            }
                        </Fragment>


                    </div>
                    
                    {/* Action edit */}
                    <div className={imageContext.ShowImgEdit ? 'layout-sticky show' : 'layout-sticky'} style={{ top: `${imageContext.clientY}px`, left: `${imageContext.clientX}px` }}>
                        <ImageEditor />
                    </div>
                    
                    {/* Overlay */}
                    {
                        imageContext.isChange ? <div style={{
                            position: 'fixed',
                            top: '0px',
                            right: '0px',
                            bottom: '0px',
                            left: '0px',
                            backgroundColor: 'rgba(0, 0, 0, 0.5)'
                        }} onClick={() => {
                            imageContext.setIsChange(false)
                        }} /> : ""
                    }

                    {/* Action element item */}
                    <div className={imageContext.isChange ? 'action-content show' : 'action-content'} style={{ top: `${imageContext.clientY}px`, left: `${imageContext.clientX}px` }}>
                        <div className="action-item">
                            {/* <a onClick={imageContext.drawI} href="#">
                                <i className="fas fa-images"></i>
                                <span>Thêm ảnh</span>
                            </a> */}
                            <label htmlFor="inputImage">
                                <i className="fas fa-images"></i>
                                <span>Thêm ảnh</span>
                                <input id="inputImage" className="hidden" type="file" onChange={(e) => {
                                    imageContext.addImageItem(URL.createObjectURL(e.target.files[0]))
                                }} />
                            </label>
                        </div>
                        <div className="action-item">
                            <a onClick={() => imageContext.setShowChange(true)} href="#">
                                <i className="fas fa-fill-drip"></i>
                                <span>Đổ màu</span>
                            </a>
                        </div>
                        {imageContext.showChange ? <ChangeColorEx type="colorItem" /> : ""}
                    </div>
                </div>
                {/* Action Prev & Next and Zoom */}
                <div className="action-control">
                    <button type="button" name="" id="" className="btn btn-undo">
                        <svg xmlns="http://www.w3.org/2000/svg" width={13} height={12} viewBox="0 0 13 12" fill="none">
                            <path d="M5.26541 0.0300853C5.11888 -0.0312641 4.94852 0.00321331 4.83545 0.115265L0.115601 3.8231C-0.0385335 3.97622 -0.0385335 4.2272 0.115601 4.38083L4.83545 8.07092C4.91049 8.14697 5.0119 8.18703 5.11381 8.18703C5.16552 8.18703 5.21673 8.17638 5.26541 8.15711C5.41295 8.09576 5.50117 7.95228 5.50117 7.79257V5.58246C10.5486 5.58246 12.7561 7.00618 12.42 11.5405C14.5581 4.28601 10.4543 2.64631 5.50117 2.64631V0.394126C5.50066 0.234415 5.41295 0.0909278 5.26541 0.0300853Z" fill="#999999" />
                        </svg>
                        <span>Undo</span>
                    </button>
                    <button type="button" name="" id="" className="btn btn-redo">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="12" viewBox="0 0 13 12" fill="none">
                            <path d="M7.73459 0.0300853C7.88112 -0.0312641 8.05148 0.00321331 8.16455 0.115265L12.8844 3.8231C13.0385 3.97622 13.0385 4.2272 12.8844 4.38083L8.16455 8.07092C8.08951 8.14697 7.9881 8.18703 7.88619 8.18703C7.83448 8.18703 7.78327 8.17638 7.73459 8.15711C7.58705 8.09576 7.49883 7.95228 7.49883 7.79257V5.58246C2.45144 5.58246 0.243876 7.00618 0.58003 11.5405C-1.55807 4.28601 2.54575 2.64631 7.49883 2.64631V0.394126C7.49934 0.234415 7.58705 0.0909278 7.73459 0.0300853Z" fill="#999999" />
                        </svg>
                        <span>Redo</span>
                    </button>
                    <div className="act-room">
                        <button type="button" name="" id="" className="btn btn-minus" onClick={() => imageContext.setArea(imageContext.area > 300 ? imageContext.area - 120 : 100)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="1" viewBox="0 0 6 1" fill="none">
                                <path d="M5.36719 0.884766H0V0H5.36719V0.884766Z" fill="#999999" />
                            </svg>
                        </button>
                        <div className="form-control">{(imageContext.area / 1200) * 100}%</div>
                        <button type="button" name="" id="" className="btn btn-plus" onClick={() => imageContext.setArea(imageContext.area < 2000 ? imageContext.area + 120 : 1200)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="7" viewBox="0 0 6 7" fill="none">
                                <path d="M3.46875 2.49023H5.84766V3.51562H3.46875V6.21094H2.37891V3.51562H0V2.49023H2.37891V0H3.46875V2.49023Z" fill="#999999" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}
