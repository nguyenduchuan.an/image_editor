import React, { useState } from 'react'
export const TextContext = React.createContext();
export const TextProvider = ({children}) => {
    const [Font, setFont] = useState('Arial')
    const [FontSize, setFontSize] = useState(12)
    const [LineHeight, setLineHeight] = useState(20)
    const [LetterSpacing, setLetterSpacing] = useState(12)
    const [FontStyle, setFontStyle] = useState(false)
    const [FontWeight, setFontWeight] = useState(false)
    const [Underline, setUnderline] = useState(false)
    const [Align, setAlign] = useState('left')
    const [FontColor, setFontColor] = useState('')
    const [BTS, setBTS] = useState(1)
    const [BTColor, setBTColor] = useState('')
    const [BorderSize, setBorderSize] = useState(1)
    const [BorderColor, setBorderColor] = useState('')
    const [BgColor, setBgColor] = useState('')
    const [ShadowColor, setShadowColor] = useState('')
    const [ShadowBlur, setShadowBlur] = useState(1)
    const [ShadowHoz, setShadowHoz] = useState(1)
    const [ShadowVel, setShadowVel] = useState(1)

    return (
        <TextContext.Provider value={{
            Font: Font,
            FontSize: FontSize,
            LineHeight: LineHeight,
            LetterSpacing: LetterSpacing,
            FontStyle: FontStyle,
            FontWeight: FontWeight,
            Underline: Underline,
            FontColor: FontColor,
            Align: Align,
            BTS: BTS,
            BTColor: BTColor,
            BorderSize: BorderSize,
            BorderColor: BorderColor,
            BgColor: BgColor,
            ShadowColor: ShadowColor,
            ShadowBlur: ShadowBlur,
            ShadowHoz: ShadowHoz,
            ShadowVel: ShadowVel,
            setFont: setFont,
            setFontSize: setFontSize,
            setLineHeight: setLineHeight,
            setLetterSpacing: setLetterSpacing,
            setFontWeight: setFontWeight,
            setFontStyle: setFontStyle,
            setUnderline: setUnderline,
            setAlign: setAlign,
            setFontColor: setFontColor,
            setBTS: setBTS,
            setBTColor: setBTColor,
            setBorderSize: setBorderSize,
            setBorderColor: setBorderColor,
            setBgColor: setBgColor,
            setShadowColor: setShadowColor,
            setShadowBlur: setShadowBlur,
            setShadowHoz: setShadowHoz,
            setShadowVel: setShadowVel
        }}>
            {children}
        </TextContext.Provider>
    )
}
