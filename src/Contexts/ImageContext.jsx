import React, { useState, useEffect } from 'react'
export const ImageContext = React.createContext();

const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
}


export const ImageProvider = ({children}) => {
    const [SideRight, setSideRight] = useState(true)
    const [Layout, setLayout] = useState([
        {i: 'a', w: 12, h: 6, t: 0, l: 0},
        {i: 'b', w: 12, h: 6, t: 6, l: 0}
    ])
    const [Background, setBackground] = useState('#fff')
    const [Padding, setPadding] = useState(10)
    const [Radius, setRadius] = useState(0)
    const [selected, setSelected] = useState('')
    const [properties, setProperties] = useState({
        
    })
    const [isChange, setIsChange] = useState(false)
    const [clientX, setclientX] = useState(0)
    const [clientY, setclientY] = useState(0)
    const [area, setArea] = useState(600)
    const [Index, setIndex] = useState(0)
    const [DefaultLayout, setDefaultLayout] = useState('')
    // State show Image Editer
    const [ShowImgEdit, setShowImgEdit] = useState(false)
    // State show change color
    const [showChange, setShowChange] = useState(false)
    // State List Text Element
    const [elmText, setElmText] = useState([])
    const [elmImage, setelmImage] = useState([])

    // Add element to Canvas
    const addElmText = (style) => {
        let text = {
            content: 'Sample',
            font: style,
            isText: true
        }
        setElmText(elmText.concat(text))
    }
    const addElmIcon = (url) => {
        let icon = {
            url: url,
            ratio: true,
            isText: false
        }
        setelmImage(elmImage.concat(icon))
    }
    // Add image and solid color item
    const addImageItem = (img) => {
        let itemSelected = document.getElementById(selected.id);
        itemSelected.style.background = `url('${img}')`;
        itemSelected.style.backgroundSize = 'cover';
        itemSelected.style.backgroundPosition = 'center center';
        itemSelected.dataset.edit = true
        setIsChange(false)
    }
    const addSolidColorItem = (color) => {
        let itemSelected = document.getElementById(selected.id);
        itemSelected.style.background = `${color}`;
    }

    // Add background Canvass 
    const addBackGround = (data) => {
        setBackground(data)
    }

    // Show group action item
    const showTypeElm = (e) => {
        setSelected(e.target)
        setclientX(e.pageX)
        setclientY(e.pageY)
        setIsChange(!isChange)
    }
    const showEditImage = (e) => {
        console.log(e.target.childNodes[0])
        setSelected(e.target)
        setclientX(e.pageX)
        setclientY(e.pageY)
        setShowImgEdit(true)
    }

    // Event delete elm
    const removeItem = (elm) => {
        elm.dataset.edit = false
        elm.style.background = `url("data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='${ Radius }' ry='${ Radius }' stroke='%23F5A623FF' stroke-width='2' stroke-dasharray='11' stroke-dashoffset='0' stroke-linecap='square'/%3e%3c/svg%3e")`;
    }

    // Event hide image picker
    useEffect(() => {
       
        document.getElementById('App').addEventListener('click', () =>{
            setShowImgEdit(false)
        });
        let elmpicker = document.querySelector('#image-picker')
        if(elmpicker){
            elmpicker.addEventListener('click',(e) =>{
                e.preventDefault()
                setShowImgEdit(true)
            })
        }
    }, [])
    return(
        <ImageContext.Provider value={{
            Layout: Layout,
            SideRight: SideRight,
            DefaultLayout: DefaultLayout,
            Background: Background,
            area: area,
            Padding: Padding,
            Radius: Radius,
            clientX: clientX,
            clientY: clientY,
            isChange: isChange,
            Index: Index,
            elmText: elmText,
            elmImage: elmImage,
            showChange: showChange,
            ShowImgEdit: ShowImgEdit,
            setArea: setArea,
            setSideRight: setSideRight,
            setRadius: setRadius,
            setPadding: setPadding,
            showTypeElm: showTypeElm,
            showEditImage: showEditImage,
            setIsChange: setIsChange,
            setIndex: setIndex,
            addBackGround: addBackGround,
            setLayout: setLayout,
            setDefaultLayout: setDefaultLayout,
            addElmText: addElmText,
            addElmIcon: addElmIcon,
            addImageItem: addImageItem,
            addSolidColorItem: addSolidColorItem,
            setShowChange: setShowChange,
            setShowImgEdit: setShowImgEdit,
            removeItem: removeItem
        }}>
            {children}
        </ImageContext.Provider>
    )
}